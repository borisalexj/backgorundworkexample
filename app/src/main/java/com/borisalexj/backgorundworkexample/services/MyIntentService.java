package com.borisalexj.backgorundworkexample.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.borisalexj.backgorundworkexample.Util;

public class MyIntentService extends IntentService {
    public static final String SERVICE_RESULT = "ServiceResult";
    public static final String RESULT_MESSAGE_KEY = "resultMessageKey";
    private static final String ACTION_CALCULATE = "ACTION_CALCULATE";
    private static final String EXTRA_INPUT = "EXTRA_INPUT";


    public MyIntentService() {
        super("MyIntentService");
        Log.d("custom", "MyIntentService MyIntentService: thread id " + android.os.Process.myTid());
    }

    public static void startActionCalculate(Context context, int input) {
        Log.d("custom", "MyIntentService startActionCalculate: thread id " + android.os.Process.myTid());
        Intent intent = new Intent(context, MyIntentService.class);
        intent.setAction(ACTION_CALCULATE);
        intent.putExtra(EXTRA_INPUT, input);
        context.startService(intent);
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("custom", "MyIntentService onHandleIntent: thread id " + android.os.Process.myTid());
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_CALCULATE.equals(action)) {
                final int input = intent.getIntExtra(EXTRA_INPUT, 1);
                handleActionCalculate(input);
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionCalculate(int input) {
        Log.d("custom", "MyIntentService handleActionCalculate: thread id " + android.os.Process.myTid());
        int result = Util.calculateFactorial(input);
        sendResult(result);
    }

    private void sendResult(int result) {
        Log.d("custom", "MyIntentService sendResult: thread id " + android.os.Process.myTid());
        Log.d("custom", "MyIntentService sendResult: result " + result);
        Intent intent = new Intent(SERVICE_RESULT);
        intent.putExtra(RESULT_MESSAGE_KEY, result);
        LocalBroadcastManager.getInstance(getApplicationContext())
                .sendBroadcast(intent);
    }

}
