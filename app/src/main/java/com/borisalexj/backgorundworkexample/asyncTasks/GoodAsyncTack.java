package com.borisalexj.backgorundworkexample.asyncTasks;

import android.os.AsyncTask;
import android.util.Log;

import com.borisalexj.backgorundworkexample.BackgroundWorkCallback;
import com.borisalexj.backgorundworkexample.Util;

import java.lang.ref.WeakReference;

public class GoodAsyncTack extends AsyncTask<Integer, Void, Integer> {

    private WeakReference<BackgroundWorkCallback> callbackWeakReference;

    public GoodAsyncTack(BackgroundWorkCallback callback) {
        Log.d("custom", "GoodAsyncTack GoodAsyncTack: thread id " + android.os.Process.myTid());
        callbackWeakReference = new WeakReference<BackgroundWorkCallback>(callback);
    }

    @Override
    protected void onPreExecute() {
        Log.d("custom", "onPreExecute: thread id " + android.os.Process.myTid());
        BackgroundWorkCallback callback = callbackWeakReference.get();
        if (callback != null) {
            callback.onAtStart();
        }
    }

    @Override
    protected Integer doInBackground(Integer... input) {
        Log.d("custom", "GoodAsyncTack doInBackground: thread id " + android.os.Process.myTid());
        Log.d("custom", "doInBackground: " + input[0]);
        int result = Util.calculateFactorial(input[0]);
        return result;
    }

    @Override
    protected void onPostExecute(Integer result) {
        Log.d("custom", "GoodAsyncTack onPostExecute: thread id " + android.os.Process.myTid());
        Log.d("custom", "onPostExecute: result " + result);
        BackgroundWorkCallback callback = callbackWeakReference.get();
        if (callback != null) {
            callback.onAtResult(result);
        }
    }

    @Override
    protected void onCancelled(Integer result) {
        Log.d("custom", "GoodAsyncTack onCancelled: thread id " + android.os.Process.myTid());
        BackgroundWorkCallback callback = callbackWeakReference.get();
        if (callback != null) {
            callback.onAtCancel();
        }
    }

    @Override
    protected void onCancelled() {
        Log.d("custom", "GoodAsyncTack onCancelled: thread id " + android.os.Process.myTid());
        BackgroundWorkCallback callback = callbackWeakReference.get();
        if (callback != null) {
            callback.onAtCancel();
        }
    }

}

