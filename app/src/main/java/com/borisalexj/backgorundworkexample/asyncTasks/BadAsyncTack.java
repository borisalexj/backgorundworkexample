package com.borisalexj.backgorundworkexample.asyncTasks;

import android.os.AsyncTask;
import android.util.Log;

import com.borisalexj.backgorundworkexample.Util;

public class BadAsyncTack extends AsyncTask<Integer, Integer, Integer> {

    @Override
    protected void onPreExecute() {
        Log.d("custom", "onPreExecute: thread id " + android.os.Process.myTid());
    }

    @Override
    protected Integer doInBackground(Integer... input) {
        Log.d("custom", "BadAsyncTack doInBackground: thread id " + android.os.Process.myTid());
        Log.d("custom", "doInBackground: input " + input[0]);
        return Util.calculateFactorial(input[0]);
    }

    @Override
    protected void onPostExecute(Integer result) {
        Log.d("custom", "BadAsyncTack onPostExecute: thread id " + android.os.Process.myTid());
        Log.d("custom", "onPostExecute: result " + result);
    }

    @Override
    protected void onCancelled(Integer integer) {
        Log.d("custom", "BadAsyncTack onCancelled: ");
    }

    @Override
    protected void onCancelled() {
        Log.d("custom", "BadAsyncTack onCancelled: ");
    }

}

