package com.borisalexj.backgorundworkexample;

import android.app.LoaderManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.borisalexj.backgorundworkexample.asyncTasks.BadAsyncTack;
import com.borisalexj.backgorundworkexample.asyncTasks.GoodAsyncTack;
import com.borisalexj.backgorundworkexample.backgroundTasks.BackgroundTask;
import com.borisalexj.backgorundworkexample.loaders.CalculationLoader;
import com.borisalexj.backgorundworkexample.services.MyIntentService;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener, BackgroundWorkCallback, LoaderManager.LoaderCallbacks<Integer> {

    private static final String MESSAGE_KEY = "message_key";
    private EditText input;
    private TextView output;
    private ProgressBar progress;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("custom", "MainActivity onReceive: thread id " + android.os.Process.myTid());
            int result = intent.getIntExtra(MyIntentService.RESULT_MESSAGE_KEY, -1);
            setResultToTextView(result);
        }
    };
    private Handler handler;
    private ExecutorService executor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        input = findViewById(R.id.input);
        output = findViewById(R.id.result);
        progress = findViewById(R.id.progress);

        executor = Executors.newFixedThreadPool(5);

    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(getApplicationContext())
                .registerReceiver(
                        receiver, new IntentFilter(MyIntentService.SERVICE_RESULT));
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getApplicationContext())
                .unregisterReceiver(receiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        executor.shutdown();
    }

    private int getInput() {
        Log.d("custom", "MainActivity getInput: thread id " + android.os.Process.myTid());
        return Integer.valueOf(input.getText().toString());
    }

    private void setResultToTextView(Integer result) {
        setResultToTextView(String.valueOf(result));
    }

    private void setResultToTextView(final String result) {
        Log.d("custom", "MainActivity setResultToTextView: thread id " + android.os.Process.myTid());
        progress.setVisibility(View.VISIBLE);
        output.post(new Runnable() {
            @Override
            public void run() {
                output.setText(result);
            }
        });
    }

    @Override
    public void onClick(View view) {
        int input = getInput();
        progress.setVisibility(View.VISIBLE);
        setResultToTextView("----------");
        switch (view.getId()) {
            case R.id.foreground: {
                foreground(input);
                break;
            }
            case R.id.post_delayed: {
                postDelayed();
                break;
            }
            case R.id.handler: {
                useHandler(input);
                break;
            }
            case R.id.thread_executor: {
                userThreadExecutor(input);
                break;
            }
            case R.id.async_task_wrong_way: {
                useAsyncTaskInAWrongWay(input);
                break;
            }
            case R.id.async_task_good_way: {
                useAsyncTaskInAGoodWay(input);
                break;
            }
            case R.id.intent_service_plus_broadcast: {
                useIntentService(input);
                break;
            }
            case R.id.bounded_service_plus_binder: {
                Toast.makeText(this, "to be implemented", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.job_scheduler: {
                Toast.makeText(this, "to be implemented", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.loaders: {
                useLoader(input);
                break;
            }
        }
    }

    private void foreground(int input) {
        Log.d("custom", "MainActivity foreground: thread id " + android.os.Process.myTid());
        setResultToTextView(Util.calculateFactorial(input));
    }

    private void postDelayed() {
        output.postDelayed(new Runnable() {
            @Override
            public void run() {
                setResultToTextView("sample result");
            }
        }, 10000);
    }

    private void useHandler(final int input) {
        handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                Bundle bundle = msg.getData();
                int message = bundle.getInt(MESSAGE_KEY);
                setResultToTextView(message);
            }
        };
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Log.d("custom", "MainActivity useHandler run: thread id " + android.os.Process.myTid());
                int result = Util.calculateFactorial(input);
                Message message = new Message();
                Bundle bundle = new Bundle();
                bundle.putInt(MESSAGE_KEY, result);
                message.setData(bundle);
                handler.sendMessage(message);
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    private void userThreadExecutor(int input) {
        Runnable worker = new BackgroundTask(input, this);
        executor.execute(worker);
    }

    private void useAsyncTaskInAWrongWay(int input) {
        BadAsyncTack badAsyncTack = new BadAsyncTack();
        badAsyncTack.execute(input);
        try {
            Integer result = badAsyncTack.get();
            setResultToTextView(result);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void useAsyncTaskInAGoodWay(int input) {
        GoodAsyncTack goodAsyncTack = new GoodAsyncTack(this);
        goodAsyncTack.execute(input);
    }

    private void useIntentService(int input) {
        MyIntentService.startActionCalculate(this, input);
    }

    private void useLoader(int input) {
        Bundle bundle = new Bundle();
        bundle.putInt(CalculationLoader.ARGS_NUMBER, input);
        getLoaderManager().initLoader(LOADER_CALCULATION_ID, bundle, this);
        Loader<String> loader = getLoaderManager().getLoader(LOADER_CALCULATION_ID);
        loader.forceLoad();
    }

    // region background callbacks
    @Override
    public void onAtStart() {
        Log.d("custom", "onAtStart: thread id " + android.os.Process.myTid());
    }

    @Override
    public void onAtCancel() {
        Log.d("custom", "onAtCancel: thread id " + android.os.Process.myTid());
    }

    @Override
    public void onAtResult(Integer result) {
        Log.d("custom", "onAtResult: thread id " + android.os.Process.myTid());
        Log.d("custom", "onAtResult: " + result);
        setResultToTextView(result);
    }
    // end region

    // region loader
    static final int LOADER_CALCULATION_ID = 1;

    @Override
    public Loader<Integer> onCreateLoader(int id, Bundle args) {
        Log.d("custom", "MainActivity" + " onCreateLoader: " + id + "|" + LOADER_CALCULATION_ID);
        Loader<Integer> loader = null;
        if (id == LOADER_CALCULATION_ID) {
            loader = new CalculationLoader(this, args);
            Log.d("custom", "onCreateLoader: " + loader.hashCode());
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Integer> loader, Integer result) {
        Log.d("custom", "MainActivity onLoadFinished: thread id " + android.os.Process.myTid());
        Log.d("custom", "onLoadFinished for loader " + loader.hashCode()
                + ", result = " + result);
        setResultToTextView(result);
    }

    @Override
    public void onLoaderReset(Loader<Integer> loader) {
        Log.d("custom", "MainActivity onLoaderReset: thread id " + android.os.Process.myTid());
        Log.d("custom", "onLoaderReset for loader " + loader.hashCode());
    }
    // end region
}
