package com.borisalexj.backgorundworkexample;

import android.util.Log;

import java.util.concurrent.TimeUnit;

public class Util {
    public static int calculateFactorial(int number) {
        Log.d("custom", "Util calculateFactorial: thread id " + android.os.Process.myTid());
        int result = 1;
        Log.d("custom", "calculateFactorial: input " + number);
        for (int i = 1; i <= number; i++) {
            try {
                TimeUnit.MILLISECONDS.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            result = result * i;
        }
        Log.d("custom", "calculateFactorial: " + result);
        return result;
    }
}
