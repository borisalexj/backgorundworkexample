package com.borisalexj.backgorundworkexample.backgroundTasks;

import android.util.Log;

import com.borisalexj.backgorundworkexample.BackgroundWorkCallback;
import com.borisalexj.backgorundworkexample.Util;

import java.lang.ref.WeakReference;

public class BackgroundTask implements Runnable {

    private WeakReference<BackgroundWorkCallback> callbackWeakReference;
    private int input;

    public BackgroundTask(int input, BackgroundWorkCallback callback) {
        this.input = input;
        callbackWeakReference = new WeakReference<BackgroundWorkCallback>(callback);
    }

    @Override
    public void run() {
        Log.d("custom", "BackgroundTask run: thread id " + android.os.Process.myTid());
        int result = Util.calculateFactorial(input);
        BackgroundWorkCallback callback = callbackWeakReference.get();
        if (callback != null) {
            callback.onAtResult(result);
        }
    }
}