package com.borisalexj.backgorundworkexample.loaders;

import android.content.Context;
import android.content.Loader;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.borisalexj.backgorundworkexample.Util;

public class CalculationLoader extends Loader<Integer> {

    final int PAUSE = 10;

    public final static String ARGS_NUMBER = "time_format";


    CalculateTask calculateTask;
    int input;

    public CalculationLoader(Context context, Bundle args) {
        super(context);
        Log.d("custom", hashCode() + " create CalculationLoader");
        if (args != null)
            input = args.getInt(ARGS_NUMBER);
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        Log.d("custom", hashCode() + " onStartLoading");
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
        Log.d("custom", hashCode() + " onStopLoading");
    }

    @Override
    protected void onForceLoad() {
        super.onForceLoad();
        Log.d("custom", hashCode() + " onForceLoad");
        if (calculateTask != null)
            calculateTask.cancel(true);
        calculateTask = new CalculateTask();
        calculateTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, input);
    }

    @Override
    protected void onAbandon() {
        super.onAbandon();
        Log.d("custom", hashCode() + " onAbandon");
    }

    @Override
    protected void onReset() {
        super.onReset();
        Log.d("custom", hashCode() + " onReset");
    }

    public void getResultFromTask(int result) {
        Log.d("custom", "CalculationLoader getResultFromTask: ");
        deliverResult(result);
    }

    public int getResultFromTask1(int result) {
        Log.d("custom", "CalculationLoader getResultFromTask1: ");
        deliverResult(result);
        return result;
    }

    class CalculateTask extends AsyncTask<Integer, Void, Integer> {
        @Override
        protected Integer doInBackground(Integer... params) {
            Log.d("custom", "CalculateTask doInBackground: thread id " + android.os.Process.myTid());
            Log.d("custom", CalculationLoader.this.hashCode() + " doInBackground");

            return Util.calculateFactorial(params[0]);
        }

        @Override
        protected void onPostExecute(Integer result) {
            Log.d("custom", "CalculateTask onPostExecute: thread id " + android.os.Process.myTid());
            super.onPostExecute(result);
            getResultFromTask(result);
        }
    }
}