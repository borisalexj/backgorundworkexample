package com.borisalexj.backgorundworkexample;

public interface BackgroundWorkCallback {
    void onAtStart();

    void onAtCancel();

    void onAtResult(Integer result);
}
